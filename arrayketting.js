let names = ["Groucho", "Karl", "Ingeborg"];
names.filter(function (value) {  return !value.includes("a");}) // ["Groucho", "Ingeborg"]
  .map(function (value) {return value.length;}) //[8,7]
  .sort(function (a, b) {return a.length-b.length ;})  // [7,8]
  .forEach(function(a) {console.log(a);});