let names = ["Groucho", "Karl", "Ingeborg"];
names.filter(value =>  !value.includes("a")) // ["Groucho", "Ingeborg"]
  .map(value => value.length) //[8,7]
  .sort((a, b) => a.length-b.length )  // [7,8]
  .forEach(a => console.log(a));