var herProfile = {
	name: "Elisabeth",
	age: 36
};
var name = herProfile.name;
var age = herProfile.age;
console.log(name, age);
var { age, name } = herProfile;
console.log(name, age);
var { name: x, age: y } = herProfile;
console.log(x, y);
var { age = 18, familyName = "de Grote", name = "Karel" } = herProfile;
console.log(age, name, familyName);
var ar = ["een", "twee", "drie", "vier", "vijf"];
var [pink, , middenvinger] = ar;
console.log(pink, middenvinger);
var herProfile = {
	name: "Elisabeth",
	age: 36,
	address: {
		zip: 2000,
		community: "Antwerp"
	}
};
var {
	address: { community }
} = herProfile;
console.log(community); //Antwerp
