let bottle
console.log(bottle) // undefined
console.log(bottle??"bottle is empty") // bottle is empty
// commenting out the next line will abort the program
//console.log(bottle.size) // Uncaught TypeError: bottle is undefined.
console.log(bottle?.size) //undefined
console.log(bottle?.size??"1 liter") //bottle is empty1 liter

