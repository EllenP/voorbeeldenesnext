let herProfile = {
  name: "Elisabeth",
  age: 36
};
let herAddress = {
  name: "Liza",
  address: {
	zip: 2000,
	community: "Antwerp"
  }
};
let all = {...herProfile, ...herAddress}
console.log("after spread:",all);

let {name,...anonymous} = all;
console.log("name = ",name);
console.log("anonymous = ",anonymous);